echo "==> Start application"

start "" "EndlessRunnerWindows\Endless_Runner.exe"

echo "==> Wait for app to start"
sleep 5
cd TestCSharp

echo "==> Restore test project"
dotnet restore
echo "==> Run tests"
dotnet test -- xunit.parallelizeAssembly=false

echo "==> Kill app"
"taskkill //PID $(tasklist | grep Endless_Runner.exe | awk '{print $2}') //T //F"

