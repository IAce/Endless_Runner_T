namespace alttrashcat_tests_csharp.pages
{
    public class StartPage:BasePage
    {
        public StartPage(AltUnityDriver driver) : base(driver)
        {
        }
    
        public void Load(){
            Driver.LoadScene("IntroLevel");
        }
        public AltUnityObject StartButton { get => Driver.WaitForElement("straightPathLevelButton", timeout:2); }
        public AltUnityObject StartText{get => Driver.WaitForElement("rotatedPathLevelButton", timeout:2);}

        public bool IsDisplayed(){
            if(StartButton!=null && StartText!=null)
                return true;
            return false;
        }

        public void PressStart(){
            StartButton.Tap();
        }
        public string GetStartButtonText(){
            return StartButton.GetText();
        }
    }
}